﻿using UnityEngine;

public class Container : MonoBehaviour
{
	[SerializeField] private Vector2 m_size;

	[SerializeField] private Transform m_floor;
	[SerializeField] private Transform m_wall1;
	[SerializeField] private Transform m_wall2;
	[SerializeField] private Transform m_wall3;
	[SerializeField] private Transform m_wall4;

	private Transform m_transform;
	//private Quaternion m_initRotation;

	void Awake()
	{
		m_transform = this.transform;
	}

	protected void Start()
	{
		m_floor.localScale = new Vector3( m_size.x, m_size.y, 1 );
		
		m_wall1.position   = new Vector3( 0, 0.5f, ( m_size.y * 0.5f ) + 0.5f );
		m_wall1.localScale = new Vector3( m_size.x + 2, 1, 1 );

		m_wall2.position   = new Vector3( 0, 0.5f, ( -m_size.y * 0.5f ) - 0.5f );
		m_wall2.localScale = new Vector3( m_size.x + 2, 1, 1 );

		m_wall3.position   = new Vector3(( m_size.x * 0.5f ) + 0.5f, 0.5f, 0);
		m_wall3.localScale = new Vector3( 1, 1, m_size.y );
		
		m_wall4.position   = new Vector3(( -m_size.x * 0.5f ) - 0.5f, 0.5f, 0);
		m_wall4.localScale = new Vector3( 1, 1, m_size.y );

		m_transform.rotation = PicoVRManager.SDK.GetBoxQuaternion() * GetQuaternionGyro(PicoVRManager.SDK.GetBoxSensorGyr());
	}

	protected void Update()
	{
		// if(!transform.eulerAngles.Equals(PicoVRManager.SDK.GetBoxSensorGyr()))
		// {
		// 	OnScreenDebug.Log(PicoVRManager.SDK.GetBoxSensorGyr().ToString());
		// }

		if(Input.GetKeyDown(KeyCode.Joystick1Button5))
		{
			// OnScreenDebug.Log("Pico Quaternion: " + PicoVRManager.SDK.getBoxQuaternion().ToString());
			OnScreenDebug.Log("Pico Accelerometer: " + PicoVRManager.SDK.GetBoxSensorAcc().ToString());
		}

		// affects x and z axes only
		// m_transform.eulerAngles += PicoGyroToUnityEuler(PicoVRManager.SDK.GetBoxSensorGyr());

		// using quaternions
		//m_transform.rotation *= GetQuaternionGyro(PicoVRManager.SDK.GetBoxSensorGyr());
		m_transform.rotation = PicoVRManager.SDK.GetBoxQuaternion();
	}

	private Vector3 PicoGyroToUnityEuler(Vector3 p_v3Gyro)
	{
		return new Vector3(p_v3Gyro.y, 0, -p_v3Gyro.x);
	}

	// source: http://www.kircherelectronics.com/blog/index.php/11-android/sensors/15-android-gyroscope-basics
	private Quaternion GetQuaternionGyro(Vector3 p_v3Gyro)
	{
		// Time.deltaTime
		float gyroMagnitude = p_v3Gyro.magnitude;
		      p_v3Gyro      = p_v3Gyro.normalized;

		float halfTheta = gyroMagnitude * Time.deltaTime;// * 0.5f;
		float halfSin   = Mathf.Sin(halfTheta);
		float halfCos   = Mathf.Cos(halfTheta);

		// return new Quaternion(p_v3Gyro.x * halfSin, p_v3Gyro.y * halfSin, p_v3Gyro.z * halfSin, halfCos);
		return new Quaternion(p_v3Gyro.y * halfSin, p_v3Gyro.z * -halfSin * 0.5f, -p_v3Gyro.x * halfSin, halfCos);
	}
}
