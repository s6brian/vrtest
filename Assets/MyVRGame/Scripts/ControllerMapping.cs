﻿using System.Collections.Generic;
using UnityEngine;

public class ControllerMapping : MonoBehaviour
{
	/* Key Codes...
	 - Already documented (Pico SDK doc)
	 	- UpArrow, RightArrow, DownArrow, LeftArrow
	 	- Escape (back), Menu (menu), JoyStick1Button0 (A)
	 
	 - To be tested
	 	- JoyStick1Button0 - JoyStick1Button19
		- Keypad0 - Keypad9
	 */

	[SerializeField] private GameObject m_button;
	private List<KeyCode> m_listKeyCodes = new List<KeyCode>();
	private Dictionary<KeyCode, CustomButton> m_listCustomButton = new Dictionary<KeyCode,CustomButton>();

	private Dictionary<KeyCode, string> m_listDefaultKeyLabels = new Dictionary<KeyCode, string>
	{
		#if UNITY_EDITOR
		{KeyCode.A         , "A"          },
		#endif
		{KeyCode.UpArrow   , "Up Arrow"   },
		{KeyCode.DownArrow , "Down Arrow" },
		{KeyCode.RightArrow, "Right Arrow"},
		{KeyCode.LeftArrow , "Left Arrow" },
		{KeyCode.Escape    , "Escape"     },
		{KeyCode.Menu      , "Menu"       }
	};

	private const string JOYSTICK_LABEL = "JoyStick1Button";
	private const string KEYPAD_LABEL   = "Keypad";

	protected void Start ()
	{
		GameObject obj;
		CustomButton customButon;
		Transform parent = this.transform;

		// setup default keycodes
		foreach(KeyValuePair<KeyCode, string> defaultButton in m_listDefaultKeyLabels)
		{
			obj = Instantiate<GameObject>( m_button, parent );
			obj.name = defaultButton.Value;

			Vector3 localPosition = obj.transform.localPosition;
			localPosition.z = 0;
			obj.transform.localPosition = localPosition;

			obj.transform.localScale = Vector3.one;
			obj.transform.localEulerAngles = Vector3.zero;

			customButon = obj.GetComponent<CustomButton>();
			customButon.Setup(defaultButton.Value);

			m_listKeyCodes.Add(defaultButton.Key);
			m_listCustomButton.Add(defaultButton.Key, customButon);
		}

		// setup joystick1 buttons
		for(int idx = 0; idx < 20; ++idx)
		{
			obj = Instantiate<GameObject>( m_button, parent );
			obj.name = JOYSTICK_LABEL + idx.ToString();

			Vector3 localPosition = obj.transform.localPosition;
			localPosition.z = 0;
			obj.transform.localPosition = localPosition;

			obj.transform.localScale = Vector3.one;
			obj.transform.localEulerAngles = Vector3.zero;

			customButon = obj.GetComponent<CustomButton>();
			customButon.Setup(obj.name);

			m_listKeyCodes.Add(KeyCode.Joystick1Button0 + idx);
			m_listCustomButton.Add((KeyCode)(KeyCode.Joystick1Button0 + idx), customButon);
		}

		// setup keypad button
		for(int idx = 0; idx < 10; ++idx)
		{
			obj = Instantiate<GameObject>( m_button, parent );
			obj.name = KEYPAD_LABEL + idx.ToString();

			Vector3 localPosition = obj.transform.localPosition;
			localPosition.z = 0;
			obj.transform.localPosition = localPosition;

			obj.transform.localScale = Vector3.one;
			obj.transform.localEulerAngles = Vector3.zero;

			customButon = obj.GetComponent<CustomButton>();
			customButon.Setup(obj.name);

			m_listKeyCodes.Add(KeyCode.Keypad0 + idx);
			m_listCustomButton.Add((KeyCode)(KeyCode.Keypad0 + idx), customButon);
		}
	}
	
	protected void Update ()
	{
		// loop through keycodes
		foreach(KeyCode keycode in m_listKeyCodes)
		{
			if(Input.GetKeyDown(keycode))
			{
				m_listCustomButton[keycode].OnPressDown();
			}

			if(Input.GetKeyUp(keycode))
			{
				m_listCustomButton[keycode].OnPressUp();
			}
		}
	}
}
