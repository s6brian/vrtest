﻿using UnityEngine;
using UnityEngine.UI;

public class CustomButton : MonoBehaviour
{
	[SerializeField] private Image m_imageBackground;
	[SerializeField] private Text  m_textLabel;

	public void Setup(string p_strName)
	{
		m_textLabel.text = p_strName;
	}

	public void OnPressDown()
	{
		//OnScreenDebug.Log( "Press " + Time.deltaTime);
		m_imageBackground.color = Color.yellow;
	}

	public void OnPressUp()
	{
		m_imageBackground.color = Color.white;
	}

	public void Quit()
	{
		Application.Quit();
	}
}
