﻿using UnityEngine;
using UnityEngine.UI;

public class OnScreenDebug : MonoBehaviour
{
	[SerializeField] private Text m_textLog;
    static private string m_strLog;
    static bool m_bWillUpdate;

    protected void Start()
    {
        m_bWillUpdate = false;
        Log( "log started..." );
    }

    protected void Update()
    {
        if(m_bWillUpdate)
        {
            m_textLog.text = m_strLog;
            m_bWillUpdate = false;
        }
    }

    public void ClearLogs()
    {
        m_bWillUpdate = true;
        m_strLog = "\n$:\\> ";
    }

    static public void Log(string p_strLog)
    {
        m_bWillUpdate = true;
        m_strLog += "\n$:\\> " + p_strLog;

        // limit string length
        if(m_strLog.Length >= 5000)
        {
            int len = m_strLog.Length / 2;
            int idx = m_strLog.Length - len;
            m_strLog = m_strLog.Substring(idx, len);
        }
    }
}
