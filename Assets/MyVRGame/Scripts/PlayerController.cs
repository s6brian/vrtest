﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float m_speed;
    private Rigidbody m_rigidBody;

    protected void Start()
    {
       m_rigidBody = this.GetComponent<Rigidbody>(); 
    }
	
	protected void Update ()
    {
        // // get input data from keyboard or controller
        // float moveHorizontal = Input.GetAxis("Horizontal");
        // float moveVertical = Input.GetAxis("Vertical");

        // // update player position based on input
        // Vector3 position = transform.position;
        // position.x += moveHorizontal * m_speed * Time.deltaTime;
        // position.z += moveVertical * m_speed * Time.deltaTime;
        // transform.position = position;
        
        // on press 'Spacebar' or 'X' to release ball
        if(Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Joystick1Button2))
        {
            m_rigidBody.AddForce(Vector3.up * 3, ForceMode.Impulse);
            //iTween.RotateBy(this.gameObject, iTween.Hash("y",  1.0f, "speed", 1.0f, "easeType", "easeInSine"));
        }
	}
}
