﻿using UnityEngine;

#if UNITY_EDITOR
[ExecuteInEditMode]
#endif
public class RoomManager : MonoBehaviour
{
	[SerializeField] private Vector3 m_roomPosition;

	[SerializeField] private Transform m_room;
	[SerializeField] private Transform m_picoVR;
	[SerializeField] private Transform m_light;

	private Vector3 m_prevPosition;

	protected void Start ()
	{
		m_room.position   = new Vector3( m_roomPosition.x, m_roomPosition.y        , m_roomPosition.z );
		m_picoVR.position = new Vector3( m_roomPosition.x, m_roomPosition.y + 5.7f , m_roomPosition.z );
		// m_light.position  = new Vector3( m_roomPosition.x, m_roomPosition.y + 4.18f, m_roomPosition.z );

		m_prevPosition = m_roomPosition;
	}

	protected void Update()
	{
		// OnScreenDebug.Log("pico vr rotation: " + m_picoVR.transform.eulerAngles);

		// L + arow keys || left shift + arrow keys
		#if UNITY_EDITOR // || UNITY_STANDALONE
		if(Input.GetKey(KeyCode.LeftShift))
		{
			if(Input.GetKey(KeyCode.LeftArrow))
			{
				iTween.RotateBy(m_picoVR.gameObject, iTween.Hash("y", -0.01f, "time", 0.1f, "easeType", "linear"));
			}
			
			if(Input.GetKey(KeyCode.RightArrow))
			{
				iTween.RotateBy(m_picoVR.gameObject, iTween.Hash("y",  0.01f, "time", 0.1f, "easeType", "linear"));
			}

			if(Input.GetKey(KeyCode.UpArrow))
			{
				iTween.RotateBy(m_picoVR.gameObject, iTween.Hash("x", -0.01f, "time", 0.1f, "easeType", "linear"));
			}

			if(Input.GetKey(KeyCode.DownArrow))
			{
				iTween.RotateBy(m_picoVR.gameObject, iTween.Hash("x",  0.01f, "time", 0.1f, "easeType", "linear"));
			}
		}
		#else
		if(Input.GetKey(KeyCode.Joystick1Button4))
		{
			float hAxis = Input.GetAxis("Horizontal");
			float vAxis = Input.GetAxis("Vertical");

			if(hAxis != 0)
			{
				iTween.RotateBy(m_picoVR.gameObject, iTween.Hash("y", 0.01f * hAxis, "time", 0.1f, "easeType", "linear"));
			}

			if(vAxis != 0)
			{
				iTween.RotateBy(m_picoVR.gameObject, iTween.Hash("x", -0.01f * vAxis, "time", 0.1f, "easeType", "linear"));
			}
		}
		#endif

		#if UNITY_EDITOR
		if( m_prevPosition != m_roomPosition )
		{
			m_room.position   = new Vector3( m_roomPosition.x, m_roomPosition.y        , m_roomPosition.z );
			m_picoVR.position = new Vector3( m_roomPosition.x, m_roomPosition.y + 5.7f , m_roomPosition.z );
			// m_light.position  = new Vector3( m_roomPosition.x, m_roomPosition.y + 4.18f, m_roomPosition.z );
		}
		#endif
	}
}
